﻿var app = angular.module('myApp', ['angularUtils.directives.dirPagination', 'angularSpinkit']);

app.controller('mainController', function ($scope, $http) {
    var url = 'https://spreadsheets.google.com/feeds/list/1DFplmW0XPZ0DmwDn85rWmFq8NLl2_ViuTfZbDqrnLKM/1/public/values?alt=json'
    var parse = function (entry) {

        var employeeNo = entry['gsx$employeeno']['$t'];
        var firstName = entry['gsx$firstname']['$t'];
        var lastName = entry['gsx$lastname']['$t'];
        var nickName = entry['gsx$nickname']['$t'];
        var position = entry['gsx$position']['$t'];
        var department = entry['gsx$department']['$t'];
        var telephoneno = entry['gsx$telephoneno']['$t'];
        var carplate = entry['gsx$carplate']['$t'];
        //var lastedUpdatedDate = entry[]

        return {
            employeeNo: employeeNo,
            firstName: firstName,
            lastName: lastName,
            nickName: nickName,
            position: position,
            department: department,
            telephoneno: telephoneno,
            carplate: carplate
        };
    }

    $scope.employeeData = [];
    $scope.lastedUpdate = "";
    $scope.prograssing = true; 
    // Active Loader

    $http.get(url)
        .success(function (response) {
            var entries = response['feed']['entry'];
            for (key in entries) {
                var content = entries[key];
                $scope.employeeData.push(parse(content));
                $scope.prograssing = false;
            }

            //console.log();
            $scope.lastedUpdate = response['feed']['updated']['$t'];
                 
        });

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});


