 $(document).ready(function() {
    var table = $('#table');
    table.removeClass('table-bordered').addClass('table-bordered');
    table.removeClass('table-striped').addClass('table-striped');
    table.removeClass('table-hover').addClass('table-hover');
    table.removeClass(/^table-mc-/).addClass('table-mc-light-green');
   
});
 
(function(removeClass) {

	jQuery.fn.removeClass = function( value ) {
		if ( value && typeof value.test === "function" ) {
			for ( var i = 0, l = this.length; i < l; i++ ) {
				var elem = this[i];
				if ( elem.nodeType === 1 && elem.className ) {
					var classNames = elem.className.split( /\s+/ );

					for ( var n = classNames.length; n--; ) {
						if ( value.test(classNames[n]) ) {
							classNames.splice(n, 1);
						}
					}
					elem.className = jQuery.trim( classNames.join(" ") );
				}
			}
		} else {
			removeClass.call(this, value);
		}
		return this;
	}

})(jQuery.fn.removeClass);